import java.io.IOException;

public class Tree {
	public TreeNode root;

	public Tree (SongObject key){
		root = new TreeNode(key);
	}	

	public void checkLeft(TreeNode t, SongObject d){
		if(t.getLeft() == null){
			t.setLeft(new TreeNode(d));
		}
		else{
			insertNode(t.getLeft(),d);
		}
	}

	public void checkRight(TreeNode t, SongObject d){
		if(t.getRight() == null){
			t.setRight(new TreeNode(d));
		}
		else{
			insertNode(t.getRight(),d);
		}
	}

	public void insertNode (TreeNode t, SongObject d){
		if (d.getGenre().toLowerCase().compareTo(t.getKey().getGenre().toLowerCase()) < 0){
			checkLeft(t,d);
		}
		if(d.getGenre().toLowerCase().compareTo(t.getKey().getGenre().toLowerCase()) > 0){
			checkRight(t,d);
		}
		if(t.getKey().getGenre().toLowerCase().equals(d.getGenre().toLowerCase())){
			if (d.getArtist().toLowerCase().compareTo(t.getKey().getArtist().toLowerCase()) < 0){
				checkLeft(t, d);
			}
			if (d.getArtist().toLowerCase().compareTo(t.getKey().getArtist().toLowerCase()) > 0){
				checkRight(t, d);
			}
			if(t.getKey().getArtist().equals(d.getArtist())){
				if (d.getSong().toLowerCase().compareTo(t.getKey().getSong().toLowerCase()) < 0){
					checkLeft(t, d);
				}
				if (d.getSong().toLowerCase().compareTo(t.getKey().getSong().toLowerCase()) > 0){
					checkRight(t, d);
				}
			}
		}
	}


	private String all;
	private String genres;

	public void inOrderTraversal (TreeNode node) throws IOException{
		if(node == null){
			return ;
		}
		inOrderTraversal(node.getLeft());
		SongObject so = node.getKey();
//		if(getAll() == null) setAll(String.format("%s, %s, %s, %s", so.getSong(), so.getArtist(),so.getUrl(),so.getGenre()));
//		else setAll(getAll() + "\n" + String.format("%s, %s, %s, %s", so.getSong(), so.getArtist(),so.getUrl(),so.getGenre()));
		System.out.println(String.format("%s, %s, %s, %s", so.getSong(), so.getArtist(),so.getUrl(),so.getGenre()));
		inOrderTraversal(node.getRight());
	}

	public void inOrderTraversal(TreeNode node, String genre){
		if(node == null){
			return ;
		}
		inOrderTraversal(node.getLeft(), genre);
		SongObject so = node.getKey();
		if(so.getGenre().toLowerCase().contentEquals(genre.toLowerCase())){
			if(getGenres() == null) setGenres(String.format("%s, %s, %s, %s", so.getSong(), so.getArtist(),so.getUrl(),so.getGenre()));
			else setGenres(getGenres() + "\n" + String.format("%s, %s, %s, %s", so.getSong(), so.getArtist(),so.getUrl(),so.getGenre()));
		}
		inOrderTraversal(node.getRight(), genre);
	}

	public String getRandom(){
		String[] array = getGenres().split("\n");
		return array[(int) ((Math.random()*array.length))];
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getGenres() {
		return genres;
	}

	public void setGenres(String genres) {
		this.genres = genres;
	}
}
