import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


public class SortInTree {

	private String toGUI;
	//if true return random if false no genre
	public SortInTree(String filename, String genre, boolean selectOne){
		try {
			String[] array = getData(filename).split("\n");
			Tree tree = new Tree(new SongObject());
			for(String s: array){
				if(!s.equals(null)){
					String [] data = s.split(",");
					tree.insertNode(tree.root, new SongObject(data[0],data[1],data[2],data[3]));
				}
			}
			tree.inOrderTraversal(tree.root, genre);
			if(selectOne) toGUI = tree.getRandom();
			else toGUI = tree.getGenres();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	//no genre returns all
	public SortInTree(String filename) {
		try {
			String[] array = getData(filename).split("\n");
			Tree tree = new Tree(new SongObject());
			for(String s: array){
				String [] data = s.split(",");
				tree.insertNode(tree.root, new SongObject(data[0],data[1],data[2],data[3]));
			}
			tree.inOrderTraversal(tree.root);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getData(String filename) throws FileNotFoundException{
		String result = "";
		try{
			URL fileU = getClass().getResource("/" + filename);
			InputStream file = fileU.openStream();
			byte[] b = new byte[file.available()];
			file.read(b);
			result = new String(b);
		}
		catch (Exception e){
			e.printStackTrace(); 
		}
		return result;	
	}

	public String getFromConstructor(){
		return toGUI;
	}

}
