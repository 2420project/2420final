import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetData {

	//csv writing stuff
	private BufferedWriter bwriter;

	//Artist URL
	private String artistURL = "http://imvdb.com/n/";
	private String artistBrowse = "http://imvdb.com/browse/artists/";
	private String video = "http://imvdb.com/video/";
	private List <String> entry = new ArrayList<String>();
	List<String[]> allMusic = Arrays.asList(entry.toArray(new String[entry.size()][]));


	public GetData(String letter){
		try{
			
			if(letter==""){
				for(char ch = 'a'; ch <= 'z'; ++ch)	get(String.valueOf(ch));
				letter = "allEntries";
			}
			else{
				get(letter.replace(" ", "_"));
			}
			
			FileOutputStream fostream = new FileOutputStream(letter+".csv");
			OutputStreamWriter oswriter = new OutputStreamWriter(fostream);
			bwriter = new BufferedWriter(oswriter);
			
			for (int i = 0; i < entry.size(); i++){
				System.out.println(i+"/"+entry.size()+" positions.");
				getArtistSong(entry.get(i));
			}
			
			System.out.println("DONE, closing csv write.");
			bwriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void get(String letter){
		Document doc;
		try {
			doc = Jsoup.connect(artistBrowse+letter).get();
//			System.out.println(doc.toString());
			Elements links = doc.select("a[href]");
			for (Element link : links) {
				if(link.attr("href").contains(artistURL)){
					entry.add(link.text());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	public List<String> getArtistSong(String artist){
		String orgArtist = artist;
		List<String> songs = new ArrayList<String>();
		System.out.println(String.format("Songs by %s:", artist));
		if(artist.toLowerCase().contains(", the"))	artist = "the-" + artist.replace(", The", "");
		artist = artist.toLowerCase().replace(" ", "-");
		Document doc;
		try{
			doc = Jsoup.connect(artistURL + artist).timeout(15000).get(); //timeout set to 15 seconds
			Elements links = doc.select("table.creditList.creditByType strong");
			for (Element link : links) {
				if(link.toString().contains(video)){ //Filtering out directors since their url's are different than the actual song urls
					csvWrite(orgArtist, link.text().replaceAll(",", ""), getYoutubeURL(link.unwrap().absUrl("href")), lastFM(orgArtist));
				}
			}
		} catch (IOException e) {
			System.out.println(e);
			System.out.println(artist + " timed out");
		}
		return songs;
	}

	public String getYoutubeURL(String songURL){
		Document doc;
		String song = null;
		try {
			doc = Jsoup.connect(songURL).get();
			Elements links = doc.select("a[href]");
			for (Element link : links) {
				if(link.attr("href").contains("youtube")){
					song = link.attr("href");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return song;
	}

	public String lastFM(String letter){
		String genere = null;
		Document doc;
		letter = letter.replaceAll(" ", "+");
		try {
			doc = Jsoup.connect("http://www.last.fm/music/"+letter).get();
			Elements links = doc.select("a[href]");
			for (Element link : links) {
				if(link.attr("href").contains("/tag/")){
					if(link.text().contentEquals("rock") || link.text().contentEquals("alternative") || link.text().contentEquals("acoustic") || link.text().contentEquals("classical") || link.text().contentEquals("trance") || link.text().contentEquals("hip-hop") || link.text().contentEquals("indie rock") || link.text().contentEquals("alternative rock") || link.text().contentEquals("instrumental") || link.text().contentEquals("heavy metal") || link.text().contentEquals("folk") || link.text().contentEquals("punk") || link.text().contentEquals("jazz") || link.text().contentEquals("rap") || link.text().contentEquals("experimental") || link.text().contentEquals("metal") || link.text().contentEquals("pop") || link.text().contentEquals("funk") || link.text().contentEquals("indie") || link.text().contentEquals("country") || link.text().contentEquals("comedy") || link.text().contentEquals("dance") || link.text().contentEquals("electronic") || link.text().contentEquals("rnb") || link.text().contentEquals("blues") || link.text().contentEquals("classic rock")){
						genere = link.text();
					}
				}
			}
		} catch (IOException e) {
			System.out.println("Artist not found on lastFM, skipping.");
		}
		return genere;
	}

	public void csvWrite(String artist, String title, String youtubeURL, String genere){
		if(genere != null && youtubeURL != null){
			System.out.println(artist+","+title+","+youtubeURL+","+genere+",");
			try {
				bwriter.write(artist+","+title+","+youtubeURL+","+genere+",");
				bwriter.newLine();
				bwriter.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		new GetData("");
	}

}