import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


public class Olympus extends JFrame {
	private static final long serialVersionUID = 1L;
	private String youtubeURL;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	public static void main(String[] args) throws IOException, InterruptedException {
		if(args.length == 0){
		//	Runtime.getRuntime().exec("cmd /k start java -jar Olympus.jar true");
			main(new String[]{"true"});
		}
		else {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						Olympus frame = new Olympus();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	}

	public Olympus() {
		String[] genres = {"Acoustic", "Alternative", "Alternative Rock", "Blues", "Comedy", "Classical", "Classic Rock", "Country", "Dance", "Electronic", "Experimental", "Folk", "Funk", "Heavy Metal", "Hip-Hop", "Indie", "Indie Rock", "Instrumental", "Jazz", "Metal", "Pop", "Punk", "Rap", "RnB", "Rock", "Trance"};
		setTitle("Olympus");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 520, 132);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblGenere = new JLabel("Genre");
		lblGenere.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGenere.setBounds(10, 11, 46, 14);
		contentPane.add(lblGenere);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(66, 39, 175, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(66, 64, 175, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		final JComboBox<Object> comboBox = new JComboBox<Object>(genres);
		comboBox.setBounds(66, 8, 175, 20);
		contentPane.add(comboBox);

		final JButton btnYoutube = new JButton("Youtube");
		btnYoutube.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					java.awt.Desktop.getDesktop().browse(java.net.URI.create(youtubeURL));
				}
				catch (java.io.IOException e) {
					System.out.println(e.getMessage());
				}
			}
		});
		btnYoutube.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnYoutube.setBounds(251, 39, 243, 44);
		contentPane.add(btnYoutube);
		btnYoutube.setEnabled(false);

		JButton btnRandom = new JButton("Random");
		btnRandom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Loading...");
				new Thread(new Runnable(){
					public void run() {
						SortInTree sort1 = new SortInTree("data.csv", comboBox.getSelectedItem().toString(), true);
						System.out.println(sort1.getFromConstructor());
						String[] songData = sort1.getFromConstructor().split(", ");
						textField.setText(songData[1]);
						textField_1.setText(songData[0]);
						youtubeURL = songData[2];
						btnYoutube.setEnabled(true);
					}
				}).start();

			}
		});
		btnRandom.setBounds(335, 7, 89, 23);
		contentPane.add(btnRandom);

		JButton btnNewButton = new JButton("Genre");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Loading...");
				new Thread(
						new Runnable(){
							public void run() {
								SortInTree sort = new SortInTree("data.csv", comboBox.getSelectedItem().toString(), false);
								System.out.println(sort.getFromConstructor());
							}
						}).start();
			}
		});
		btnNewButton.setBounds(251, 7, 74, 23);
		contentPane.add(btnNewButton);

		JLabel lblArtist = new JLabel("Artist");
		lblArtist.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblArtist.setBounds(10, 42, 46, 14);
		contentPane.add(lblArtist);

		JLabel lblSong = new JLabel("Song");
		lblSong.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSong.setBounds(10, 67, 46, 17);
		contentPane.add(lblSong);

		JButton btnNewButton_1 = new JButton("All");
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Loading...");
				new Thread(new Runnable(){
					public void run() {
						new SortInTree("data.csv");
					}
				}).start();
			}
		});
		btnNewButton_1.setBounds(434, 7, 60, 23);
		contentPane.add(btnNewButton_1);
		setVisible(true);
	}
}
