
public class SongObject {

	private String song;
	private String artist;
	private String genre;
	private String url;
	
	public SongObject() {
		super();
		this.song = "";
		this.artist = "";
		this.genre = "";
		this.url = "";
	}

	public SongObject(String artist, String song, String url, String genre) {
		super();
		this.song = song;
		this.artist = artist;
		this.genre = genre;
		this.url = url;
	}

	public String getSong() {
		return song;
	}

	public void setSong(String song) {
		this.song = song;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
