//pulls from CSV data file
public class TreeNode {

	private SongObject key;
	private TreeNode left;
	private TreeNode right;

	public TreeNode(SongObject key){
		this.key = key;
		left = right = null;
	}

	public SongObject getKey (){
		return key;
	}

	public void setKey (SongObject key){
		this.key = key;
	}

	public TreeNode getLeft(){
		return left;
	}

	public TreeNode getRight(){
		return right;
	}

	public void setLeft (TreeNode left){
		this.left = left;
	}

	public void setRight (TreeNode right){
		this.right = right;
	}
}
